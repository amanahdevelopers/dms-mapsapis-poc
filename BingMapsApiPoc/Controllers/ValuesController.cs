﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BingMapsRESTToolkit;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        const string BingMapsKey = "AvQ4jujuHXug1CKI6L8PLAUO_vBeaxHszWsOyDyQNtBNkVOgPXhhNzYjWjglos8i";

        // GET api/values
        [HttpGet("{lat1}/{lng1}/{lat2}/{lng2}")]
        public async Task<IActionResult> Get(double lat1, double lng1, double lat2, double lng2)
        {
            double? travelDurationTraffic = null;

            var req = new RouteRequest()
            {
                RouteOptions = new RouteOptions()
                {
                    Avoid = new List<AvoidType>()
                    {
                        AvoidType.MinimizeTolls
                    },
                    TravelMode = TravelModeType.Driving,
                    DistanceUnits = DistanceUnitType.Kilometers,
                    //Heading = 45,
                    RouteAttributes = new List<RouteAttributeType>()
                    {
                        RouteAttributeType.All
                    }
                    , Optimize = RouteOptimizationType.TimeWithTraffic
                },
                Waypoints = new List<SimpleWaypoint>()
                {
                    new SimpleWaypoint(){
                        Coordinate = new Coordinate{ Latitude = lat1, Longitude = lng1 }
                    },
                    //new SimpleWaypoint(){
                    //    Address = "Bellevue, WA",
                    //    IsViaPoint = true
                    //},
                    new SimpleWaypoint(){
                        Coordinate = new Coordinate{ Latitude = lat2, Longitude = lng2 }
                    }
                },
                BingMapsKey = BingMapsKey
            };

             var res = await req.Execute();
            var resource = res.ResourceSets.FirstOrDefault()?.Resources.FirstOrDefault();
            var route = resource as Route;
            if (route != null)
            {
                travelDurationTraffic = route.TravelDurationTraffic;
            }

            return Ok(new { travelDurationTraffic });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
