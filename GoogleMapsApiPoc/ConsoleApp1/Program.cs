﻿using RestSharp;
using System;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Rapid();
        }

        public static async Task Rapid()
        {
            var client = new RestClient("https://distanceto.p.rapidapi.com/get?car=false&foot=false&route=[{'t':'TXL'},{'t':'Hamburg'}]");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "distanceto.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", "92e87eee6dmshd7be848346f00dfp193f31jsne817c62cdaab");
            IRestResponse response = client.Execute(request);
        }
    }
}
