using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using GoogleMapsApi;
using GoogleMapsApi.Entities.Common;
using GoogleMapsApi.Entities.Directions.Request;
using GoogleMapsApi.Entities.Directions.Response;
using GoogleMapsApi.Entities.Geocoding.Request;
using GoogleMapsApi.Entities.Geocoding.Response;
using GoogleMapsApi.StaticMaps;
using GoogleMapsApi.StaticMaps.Entities;
using GoogleMapsApi.Entities.Elevation.Request;
using RestSharp;

namespace GoogleMapsApiDemo.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        const string ApiKey = "AIzaSyBmYlXrcgGQk3dNkxVxF9wC40W0Dggcm98";

        [HttpGet("[action]")]
        public async Task<IActionResult> Rapid()
        {
            var client = new RestClient("https://distanceto.p.rapidapi.com/get?car=true&foot=false");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "distanceto.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", "92e87eee6dmshd7be848346f00dfp193f31jsne817c62cdaab");
            IRestResponse response = client.Execute(request);

            return Ok(response);
        }

        [HttpGet("[action]/{fromLat}/{fromLng}/{toLat}/{toLng}")]
        public async Task<IActionResult> Map(double fromLat, double fromLng, double toLat, double toLng)
        {
            try
            {
                //Static class use (Directions) (Can be made from static/instance class)
                    DirectionsRequest directionsRequest = new DirectionsRequest()
                {
                    Origin = $"{fromLat},{fromLng}",
                    Destination = $"{toLat},{toLng}",
                    ApiKey = ApiKey
                };

                DirectionsResponse directions = await GoogleMaps.Directions.QueryAsync(directionsRequest);

                var dd = directions.Routes.Select(r => r.Legs.Sum(l => l.Duration.Value.TotalSeconds)).Min();

                //Instance class use (Geocode)  (Can be made from static/instance class)
                GeocodingRequest geocodeRequest = new GeocodingRequest()
                {
                    Address = "Nasr City",
                    ApiKey = ApiKey
                };
                var geocodingEngine = GoogleMaps.Geocode;
                GeocodingResponse geocode = await geocodingEngine.QueryAsync(geocodeRequest);

                // Static maps API - get static map of with the path of the directions request
                StaticMapsEngine staticMapGenerator = new StaticMapsEngine();

                //Path from previos directions request
                IEnumerable<Step> steps = directions.Routes.First().Legs.First().Steps;
                // All start locations
                IList<ILocationString> path = steps.Select(step => step.StartLocation).ToList<ILocationString>();
                // also the end location of the last step
                path.Add(steps.Last().EndLocation);

                string url = staticMapGenerator.GenerateStaticMapURL(new StaticMapRequest(new Location(fromLat, fromLng), 9, new ImageSize(800, 400))
                {
                    Pathes = new List<GoogleMapsApi.StaticMaps.Entities.Path>(){ new GoogleMapsApi.StaticMaps.Entities.Path()
    {
            Style = new PathStyle()
            {
                    Color = "red"
            },
            Locations = path
    }}
                });


                return Ok(new { directions, geocode, url });
            }
            catch(Exception ex)
            {
                return Ok(ex);
            }
        }

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
